
package Person;


public class Person
{

    
    
    private long id;        // Номер стулента в БД
    private String fam;     // Поле фамилии    
    private String im;      // Поле имени
    private String ot;      // Поле отчества 
    private String birthdate;

    public Person(long id, String fam, String im, String ot, String bithdate)
    {
        this.id = id;
        this.fam = fam;
        this.im = im;
        this.ot = ot;
        this.birthdate = bithdate;
    }

    /**
     * Читает номер записи в базе данных
     * @return 
     */
    public long getId()
    {
        return id;
    }

    /**
     * записывает  номер записи в базу данных
     * 
     * @param id Номер записи в базе данных
     */
    public void setId(long id)
    {
        this.id = id;
    }

    public String getFam()
    {
        return fam;
    }

    public void setFam(String fam)
    {
        this.fam = fam;
    }

    
    public String getIm()
    {
        return im;
    }

    
    public void setIm(String im)
    {
        this.im = im;
    }

   
    public String getOt()
    {
        return ot;
    }

    
    public void setOt(String ot)
    {
        this.ot = ot;
    }

   
    public String getBirthdate()
    {
        return birthdate;
    }

    
    public void setBirthdate(String birthdate)
    {
        if (birthdate != null)
        {
            this.birthdate = birthdate;
        }
        else
        {
            this.birthdate = "";
        }
    }
    
    /*
    public void setBirthdate(String b)
    {
        if (b != null)
        {
            birthdate = b;
        }
        else
        {
            birthdate = "";
        }
    }
    */
    
    
    
}
