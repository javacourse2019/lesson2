package database;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import students.Student;

public class DatabaseManager
{
     
    private File    databaseFile;      // Объявление класса доступа к файлу
    
    public ArrayList<Student> readDatabase(String databaseName)
    {
        ArrayList<Student> list = new ArrayList<>(); // Список студентов
        try
        {
            // Создание экземпляра класса File
            databaseFile = new File(databaseName);
            
            // Создание экземпляра класса Scanner и связывание с потоком из файла
            Scanner scanner = new Scanner(databaseFile);
            //scanner.useDelimiter(" ,.");
            
            while (scanner.hasNextLine()) // Читать строки (NextLine) до конца файла
            {
                String record = scanner.nextLine();
                System.out.println("file > "+record);  
                readDatabaseRecord(record);
                
            }   
            
            return list;
        } catch (FileNotFoundException ex)
        {
            Logger.getLogger(DatabaseManager.class.getName()).log(Level.SEVERE, null, ex);
        } 
        return null;
    }    
    
    
    
    
    public Student readDatabaseRecord(String record)
    {
        Student student = new Student();
        Scanner scanner = new Scanner(record);
        
        scanner.useDelimiter(" ");
        List<String> lexemaList = new ArrayList<>();
        
        
        
        while (scanner.hasNext())
        {
            String lexema = scanner.next();  
            //student.setId(lexema);
            student.setFam(scanner.next());
            student.setIm(scanner.next());
            student.setOt(scanner.next());
            student.setBirthdate(scanner.next());
            student.setGroup(scanner.next());
            student.setStudyYear(scanner.next());
            
            lexemaList.add(lexema);
        }    
        
        System.out.println(
                student.getFam() + "\n"+
                student.getIm() + "\n" +
                student.getOt() + "\n"+
                student.getGroup());
        
        System.out.println("LexemaList: "+lexemaList);
        
        return student;
    }        
    
    
}

          /*
            //String str = "1 Путин Владимир Владимирович 01.01.1900 ПКС-120 1";
            
            //scanner = new Scanner(str);
            
            // while (scanner.hasNext())
            //{
            String lexema = scanner.next();
            System.out.println("лексема = {"+lexema+"}");
            }
            */
            
            /*
            String lexema1 = scanner.next();
            String lexema2 = scanner.next();
            String lexema3 = scanner.next();
            
            System.out.println("лексема 1 = {"+lexema1+"}");
            System.out.println("лексема 2 = {"+lexema2+"}");
            System.out.println("лексема 3 = {"+lexema3+"}");
            
            */