
package students;

import Person.Person;


public class Student extends Person
{

    
   
    private String group;   // Поле группы
    private String studyYear; // Год обучения

    public Student(){
        super(0, "", "", "", "");
    }
    public Student(long id, String fam, String im, String ot, String bithdate)
    {
        super(id, fam, im, ot, bithdate);
    }

    /**
     * @return the group
     */
    public String getGroup()
    {
        return group;
    }

    /**
     * @param group the group to set
     */
    public void setGroup(String group)
    {
        this.group = group;
    }

    /**
     * @return the studyYear
     */
    public String getStudyYear()
    {
        return studyYear;
    }

    /**
     * @param studyYear the studyYear to set
     */
    public void setStudyYear(String studyYear)
    {
        this.studyYear = studyYear;
    }
}
